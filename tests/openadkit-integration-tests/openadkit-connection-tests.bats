#!/usr/bin/env bats
#
# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Exit when simple command fails
set -o errexit  # set -e

# Exit on error inside any functions or subshells
set -o errtrace

# Trigger error when expanding unset variables
set -o nounset  # set -u

# Do not hide errors within pipes
set -o pipefail

#
# Run-time validation tests for OpenAD Kit on a SOAFEE system
#

#
# Set generic configuration
#
export TEST_LOG_FILE="${SOAFEE_LOG_DIR}/openadkit-connection-tests.log"
export TEST_RUN_FILE="${SOAFEE_TMP_DIR}/openadkit-connection-tests.pgid"
export TEST_STDERR_FILE="${SOAFEE_LOG_DIR}/openadkit-stderr.log"
export TEST_CONTAINER_ID="${SOAFEE_LOG_DIR}/openadkit-connection-tests.id"

# Set test-suite specific configuration

OAK_TEST_IMAGE="registry.gitlab.com/soafee/soafee-test-suite/soafee-openadkit:v3.17.2"

export TEST_CLEAN_ENV="${OAK_TEST_CLEAN_ENV:=1}"

load "${SOAFEE_TOP_DIR}"/lib/integration-tests-common-funcs.sh
load "${SOAFEE_TOP_DIR}"/lib/container-engine-common-funcs.sh

# Ensure that the state of the container environment is ready for the test suite
clean_test_environment() {
  # Use the BATS_TEST_NAME env var to categorise all logging messages relating
  # to the clean-up activities
  export BATS_TEST_NAME="clean_test_environment"

  _run base_cleanup "${OAK_TEST_IMAGE}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "base_cleanup"
    return 1
  fi
}

# Runs once before the first test
setup_file() {
  _run test_suite_setup clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_setup clean_test_environment"
    return 1
  fi

  # Call without run as we might export environment variables
  extra_setup
  status="${?}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "extra_setup"
    return 1
  fi
}

# Runs after the final test
teardown_file() {
  _run test_suite_teardown clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_teardown clean_test_environment"
    return 1
  fi
}

# bats file_tags=openadkit
@test 'Check ip exist in host' {
  ip_binary=$(command -v ip 2>/dev/null || echo "")
  if [[ -z "${ip_binary}" ]]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Check ping exist in host' {
  ping_binary=$(command -v ping 2>/dev/null || echo "")
  if [[ -z "${ping_binary}" ]]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

# Ping to default gateway in host
@test 'Check ping to default gateway in host' {
  default_gateway=$(ip route show default | awk '/default/ {print $3}')
  _run ping -c 10 "${default_gateway}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Check docker exist in host' {
  docker_binary=$(command -v docker 2>/dev/null || echo "")
  if [[ -z "${docker_binary}" ]]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

# Pull the image used for the tests
@test "Pull OpenAD Kit Docker image" {
  _run retry 10 3 image_pull "${OAK_TEST_IMAGE}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

# Run a container of the given image type pulled from an external image hub, to
# execute an example workload simply consisting of an interactive shell, then
# it's removed afterwards
@test "Run a container with a persistent workload" {
  _run image_run "-itd" "${OAK_TEST_IMAGE}" "/bin/sh"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi

  # Save it here so the rest of tests can use the container id
  echo "${output}" > "${TEST_CONTAINER_ID}"
}

@test "Check the container is running" {
  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")

  _run check_container_is_running "${CONTAINER_ID}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

# Run a non-detached container that ping to default gateway
@test 'Check ping to default gateway in container' {
  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")

  engine_args="-it"
  cmd="ip route show default"

  _run container_exec "${engine_args}" "${CONTAINER_ID}" "${cmd}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  fi

  default_gateway=$(echo "${output}" | awk '/default/ {print $3}')
  cmd="ping -c 10 ${default_gateway}"

  _run container_exec "${engine_args}" "${CONTAINER_ID}" "${cmd}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}
