#!/usr/bin/env bats
#
# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Exit when simple command fails
set -o errexit  # set -e

# Exit on error inside any functions or subshells
set -o errtrace

# Trigger error when expanding unset variables
set -o nounset  # set -u

# Do not hide errors within pipes
set -o pipefail

#
# Run-time validation tests for OpenAD Kit on a SOAFEE system
#

#
# Set generic configuration
#
export TEST_LOG_FILE="${SOAFEE_LOG_DIR}/opeadkit-linux-abi-tests.log"
export TEST_RUN_FILE="${SOAFEE_TMP_DIR}/opeadkit-linux-abi-tests.pgid"
export TEST_STDERR_FILE="${SOAFEE_LOG_DIR}/openadkit-stderr.log"

# Set test-suite specific configuration

export TEST_CLEAN_ENV=0

load "${SOAFEE_TOP_DIR}"/lib/integration-tests-common-funcs.sh

# Runs once before the first test
setup_file() {
  _run test_suite_setup
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_setup"
    return 1
  fi
}

# Runs after the final test
teardown_file() {
  _run test_suite_teardown
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_teardown"
    return 1
  fi
}

# bats file_tags=openadkit
@test 'Check ABI smoke Test Suite' {
  LTP_RESULT_FILE="${SOAFEE_LOG_DIR}/ltp-results.log"
  LTP_RESULT_OUTPUT_FILE="${SOAFEE_LOG_DIR}/ltp-results-output.log"
  LTP_RESULT_FAILED_FILE="${SOAFEE_LOG_DIR}/ltp-results-failed.log"

  # Run a pre-defined Smoke Test Suite
  _run sudo /opt/ltp/runltp \
    -p -q \
    -f "${BATS_TEST_DIRNAME}/ltp-smoke-tests" \
    -l "${LTP_RESULT_FILE}" \
    -o "${LTP_RESULT_OUTPUT_FILE}" \
    -C "${LTP_RESULT_FAILED_FILE}" \
    -d "${SOAFEE_LOG_DIR}"

  if [ "${status}" -ne 0 ]; then
    LTP_RESULT_ONE_FILE="${SOAFEE_LOG_DIR}/ltp-results-one.log"

    # Put together all the ltp logs for error analysis
    cat "$LTP_RESULT_FILE" "$LTP_RESULT_OUTPUT_FILE" "$LTP_RESULT_FAILED_FILE" \
      > "$LTP_RESULT_ONE_FILE"

    log "FAIL" "${BATS_TEST_NAME}" "${LTP_RESULT_ONE_FILE}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

