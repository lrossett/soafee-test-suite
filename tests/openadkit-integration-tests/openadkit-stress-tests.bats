#!/usr/bin/env bats
#
# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Exit when simple command fails
set -o errexit  # set -e

# Exit on error inside any functions or subshells
set -o errtrace

# Trigger error when expanding unset variables
set -o nounset  # set -u

# Do not hide errors within pipes
set -o pipefail

#
# Run-time validation tests for OpenAD Kit on a SOAFEE system
#

#
# Set generic configuration
#
export TEST_LOG_FILE="${SOAFEE_LOG_DIR}/opeadkit-stress-tests.log"
export TEST_RUN_FILE="${SOAFEE_TMP_DIR}/opeadkit-stress-tests.pgid"
export TEST_STDERR_FILE="${SOAFEE_LOG_DIR}/openadkit-stderr.log"
export TEST_CONTAINER_ID="${SOAFEE_LOG_DIR}/openadkit-stress-tests.id"

# Set test-suite specific configuration

OAK_TEST_IMAGE="registry.gitlab.com/soafee/soafee-test-suite/soafee-openadkit:v3.17.2a"

export TEST_CLEAN_ENV="${OAK_TEST_CLEAN_ENV:=1}"

export STRESS_RESULT_FILE="${SOAFEE_LOG_DIR}/stress-ng-results.log"
export STRESS_TEMP_DIR="${SOAFEE_LOG_DIR}"/stress-ng

# Time the stress tests will run
export STRESS_TIMEOUT="60s"

load "${SOAFEE_TOP_DIR}"/lib/integration-tests-common-funcs.sh
load "${SOAFEE_TOP_DIR}"/lib/container-engine-common-funcs.sh

# Ensure that the state of the container environment is ready for the test suite
clean_test_environment() {
  # Use the BATS_TEST_NAME env var to categorise all logging messages relating
  # to the clean-up activities
  export BATS_TEST_NAME="clean_test_environment"

  _run base_cleanup "${OAK_TEST_IMAGE}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "base_cleanup"
    return 1
  fi
}

# Runs once before the first test
setup_file() {
  _run test_suite_setup clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_setup clean_test_environment"
    return 1
  fi

  # Call without run as we might export environment variables
  extra_setup
  status="${?}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "extra_setup"
    return 1
  fi

  _run mkdir "${STRESS_TEMP_DIR}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "mkdir ${STRESS_TEMP_DIR}"
    return 1
  fi
}

# Runs after the final test
teardown_file() {
  if [[ -d "${STRESS_TEMP_DIR}" ]]; then
    _run rmdir "${STRESS_TEMP_DIR}"
    if [ "${status}" -ne 0 ]; then
      log "FAIL" "rmdir ${STRESS_TEMP_DIR}"
      return 1
    fi
  fi

  _run test_suite_teardown clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_teardown clean_test_environment"
    return 1
  fi
}

# bats file_tags=openadkit
@test 'Check stress-ng exist' {
  if ! has_cmd stress-ng; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

run_stress_ng() {
  local timeout
  timeout=$1
  shift

  _run sudo stress-ng \
    --verify \
    --metrics-brief \
    --log-brief \
    --times \
    --tz \
    --temp-path "${STRESS_TEMP_DIR}" \
    --log-file "${STRESS_RESULT_FILE}" \
    --timeout "${timeout}" \
    "$@" > "${TEST_LOG_FILE}" 2>&1

  return $?
}

# Required by docker
export -f run_stress_ng

@test 'Stress CPU for one minute in host' {
  ! has_cmd stress-ng && skip 'stress-ng is not installed'

  if ! run_stress_ng "${STRESS_TIMEOUT}" --cpu -1; then
    log "FAIL" "${BATS_TEST_NAME}" "${STRESS_RESULT_FILE}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Stress IO for one minute in host' {
  ! has_cmd stress-ng && skip 'stress-ng is not installed'

  if ! run_stress_ng "${STRESS_TIMEOUT}" --iomix -1 --iomix-bytes 10%; then
    log "FAIL" "${BATS_TEST_NAME}" "${STRESS_RESULT_FILE}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Stress Virtual Memory for one minute in host' {
  ! has_cmd stress-ng && skip 'stress-ng is not installed'

  if ! run_stress_ng "${STRESS_TIMEOUT}" --vm -1; then
    log "FAIL" "${BATS_TEST_NAME}" "${STRESS_RESULT_FILE}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Stress Interruptions for one minute in host' {
  ! has_cmd stress-ng && skip 'stress-ng is not installed'

  if ! run_stress_ng "${STRESS_TIMEOUT}" --aio -1; then
    log "FAIL" "${BATS_TEST_NAME}" "${STRESS_RESULT_FILE}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Stress Networking for one minute in host' {
  ! has_cmd stress-ng && skip 'stress-ng is not installed'

  if ! run_stress_ng "${STRESS_TIMEOUT}" --netdev -1; then
    log "FAIL" "${BATS_TEST_NAME}" "${STRESS_RESULT_FILE}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Stress together CPU, IO, VM, Interruptions & Networking for one minute in host' {
  ! has_cmd stress-ng && skip 'stress-ng is not installed'

  local args
  args=("${STRESS_TIMEOUT}")

  # -1 means all available resources
  args+=(--parallel -1)
  args+=(--cpu -1)
  args+=(--iomix -1)
  args+=(--vm -1)
  args+=(--aio -1)
  args+=(--netdev -1)

  if ! run_stress_ng "${args[@]}"; then
    log "FAIL" "${BATS_TEST_NAME}" "${STRESS_RESULT_FILE}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Check docker exist in host' {
  if ! has_cmd docker; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

# Pull the image used for the tests
@test "Pull OpenAD Kit Docker image" {
  ! has_cmd docker && skip 'docker is not installed'

  _run retry 10 3 image_pull "${OAK_TEST_IMAGE}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test "Run a persistent stress-ng container" {
  ! has_cmd docker && skip 'docker is not installed'

  _run docker run -itd -e run_stress_ng "${OAK_TEST_IMAGE}" "/bin/sh"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi

  # Save it here so the rest of tests can use the container id
  echo "${output}" > "${TEST_CONTAINER_ID}"
}

@test "Check stress-ng container is running" {
  ! has_cmd docker && skip 'docker is not installed'

  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")

  _run check_container_is_running "${CONTAINER_ID}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Stress CPU for one minute in stress-ng container' {
  ! has_cmd docker && skip 'docker is not installed'

  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")

  engine_args="-it"

  _run container_exec "${engine_args}" "${CONTAINER_ID}" \
    stress-ng -t "${STRESS_TIMEOUT}" --cpu -1
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Stress IO for one minute in stress-ng container' {
  ! has_cmd docker && skip 'docker is not installed'

  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")

  engine_args="-it"

  _run container_exec "${engine_args}" "${CONTAINER_ID}" \
    stress-ng -t "${STRESS_TIMEOUT}" --iomix -1
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Stress Virtual Memory for one minute in stress-ng container' {
  ! has_cmd docker && skip 'docker is not installed'

  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")

  engine_args="-it"

  _run container_exec "${engine_args}" "${CONTAINER_ID}" \
    stress-ng -t "${STRESS_TIMEOUT}" --vm -1
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Stress Interruptions for one minute in stress-ng container' {
  ! has_cmd docker && skip 'docker is not installed'

  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")

  engine_args="-it"

  _run container_exec "${engine_args}" "${CONTAINER_ID}" \
    stress-ng -t "${STRESS_TIMEOUT}" --aio -1
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Stress Networking for one minute in stress-ng container' {
  ! has_cmd docker && skip 'docker is not installed'

  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")

  engine_args="-it"

  _run container_exec "${engine_args}" "${CONTAINER_ID}" \
    stress-ng -t "${STRESS_TIMEOUT}" --netdev -1
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}
