#!/usr/bin/env bats
#
# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Exit when simple command fails
set -o errexit  # set -e

# Exit on error inside any functions or subshells
set -o errtrace

# Trigger error when expanding unset variables
set -o nounset  # set -u

# Do not hide errors within pipes
set -o pipefail

#
# Run-time validation tests for OpenAD Kit on a SOAFEE system
#

#
# Set generic configuration
#
export TEST_LOG_FILE="${SOAFEE_LOG_DIR}/opeadkit-vga-tests.log"
export TEST_RUN_FILE="${SOAFEE_TMP_DIR}/opeadkit-vga-tests.pgid"
export TEST_STDERR_FILE="${SOAFEE_LOG_DIR}/openadkit-stderr.log"

# Set test-suite specific configuration

export TEST_CLEAN_ENV=0

load "${SOAFEE_TOP_DIR}"/lib/integration-tests-common-funcs.sh

# Runs once before the first test
setup_file() {
  _run test_suite_setup
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_setup"
    return 1
  fi
}

# Runs after the final test
teardown_file() {
  _run test_suite_teardown
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_teardown"
    return 1
  fi
}

# bats file_tags=openadkit
@test 'Check systemd-detect-virt exist' {
  binary=$(command -v systemd-detect-virt 2>/dev/null || echo "")
  if [[ -z "${binary}" ]]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Check lspci exist' {
  lspci_binary=$(command -v lspci 2>/dev/null || echo "")
  if [[ -z "${lspci_binary}" ]]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Check DUT has a VGA controller' {
  if is_ava; then
    if ! has_vga; then
      log "FAIL" "${BATS_TEST_NAME}"
      return 1
    else
      log "PASS" "${BATS_TEST_NAME}"
    fi
  else
    skip "DUT is not an AVA HW"
  fi
}

