#!/usr/bin/env bats
#
# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Exit when simple command fails
set -o errexit # set -e

# Exit on error inside any functions or subshells
set -o errtrace

# Trigger error when expanding unset variables
set -o nounset # set -u

# Do not hide errors within pipes
set -o pipefail

#
# Run-time validation tests for OpenAD Kit on a SOAFEE system
#

#
# Set generic configuration
#
export TEST_LOG_FILE="${SOAFEE_LOG_DIR}/openadkit-image-tests.log"
export TEST_RUN_FILE="${SOAFEE_TMP_DIR}/openadkit-image-tests.pgid"
export TEST_STDERR_FILE="${SOAFEE_LOG_DIR}/openadkit-stderr.log"

# Set test-suite specific configuration

# @FIXME: use a specific image version to guarantee reproducibility
OAK_TEST_IMAGE="registry.gitlab.com/autowarefoundation/autoware.auto/autowareauto/arm64/openadkit-foxy:latest"

export TEST_CLEAN_ENV="${OAK_TEST_CLEAN_ENV:=1}"

load "${SOAFEE_TOP_DIR}"/lib/integration-tests-common-funcs.sh
load "${SOAFEE_TOP_DIR}"/lib/container-engine-common-funcs.sh

# Ensure that the state of the container environment is ready for the test suite
clean_test_environment() {
  # Use the BATS_TEST_NAME env var to categorise all logging messages relating
  # to the clean-up activities
  export BATS_TEST_NAME="clean_test_environment"

  _run base_cleanup "${OAK_TEST_IMAGE}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "base_cleanup"
    return 1
  fi
}

# Runs once before the first test
setup_file() {
  _run test_suite_setup clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_setup clean_test_environment"
    return 1
  fi

  # Call without run as we might export environment variables
  extra_setup
  status="${?}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "extra_setup"
    return 1
  fi
}

# Runs after the final test
teardown_file() {
  _run test_suite_teardown clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_teardown clean_test_environment"
    return 1
  fi
}

# bats file_tags=openadkit
@test 'Pull OpenAD Kit Demo image' {
  if is_ava; then
    _run retry 10 3 image_pull "${OAK_TEST_IMAGE}"
    if [ "${status}" -ne 0 ]; then
      log "FAIL" "${BATS_TEST_NAME}"
      return 1
    else
      log "PASS" "${BATS_TEST_NAME}"
    fi
  else
    skip "DUT is not an AVA HW"
  fi
}

# Run a container with a persistent workload
@test 'Create a container from OpenAD Kit image' {
  if is_ava; then
    _run image_run "-itd" "${OAK_TEST_IMAGE}" "/bin/sh"
    if [ "${status}" -ne 0 ]; then
      log "FAIL" "${BATS_TEST_NAME}"
      return 1
    else
      log "PASS" "${BATS_TEST_NAME}"
    fi
  else
    skip "DUT is not an AVA HW"
  fi
}
