#!/usr/bin/env bash
#
# Copyright (c) 2022, Arm Limited.
# Copyright (c) 2022, Linaro Limited.
#
# SPDX-License-Identifier: MIT

check_user_home_dir_mode() {
  dirmode="$(stat -c "%A" "/home/${1}" 2>"${TEST_STDERR_FILE}")"
  if [ "${dirmode:4}" = "------" ]; then
    return 0
  else
    echo "Directory '/home/${1}' current mode: '${dirmode}'," \
      "expected mode: 'drwx------'."
    return 1
  fi
}

check_user_sudo_privileges() {
  # No the more accurate way to check for sudo permissions, but for a
  # controlled environment is enough
  id "${1}" | grep --quiet -E "(wheel|sudo)"
  return $?
}
