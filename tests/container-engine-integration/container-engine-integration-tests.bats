#!/usr/bin/env bats
#
# Copyright (c) 2021-2022, Arm Limited.
# Copyright (c) 2022, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Exit when simple command fails
set -o errexit  # set -e

# Exit on error inside any functions or subshells
set -o errtrace

# Trigger error when expanding unset variables
set -o nounset  # set -u

# Do not hide errors within pipes
set -o pipefail

# Run-time validation tests for the container engine running on a EWAOL system

# Set generic configuration

export TEST_LOG_FILE="${SOAFEE_LOG_DIR}/container-engine-integration-tests.log"
export TEST_STDERR_FILE="${SOAFEE_LOG_DIR}/ce-stderr.log"
export TEST_RUN_FILE="${SOAFEE_TMP_DIR}/container-engine-integration-tests.pgid"
export TEST_CONTAINER_ID="${SOAFEE_LOG_DIR}/container-engine-integration-tests.id"

# Set test-suite specific configuration

CE_TEST_IMAGE="registry.gitlab.com/soafee/soafee-test-suite/soafee-alpine:v3.17.2"

export TEST_CLEAN_ENV="${CE_TEST_CLEAN_ENV:=1}"

load "${SOAFEE_TOP_DIR}"/lib/integration-tests-common-funcs.sh
load "${SOAFEE_TOP_DIR}"/lib/container-engine-common-funcs.sh

# Global container id used by the tests

# Ensure that the state of the container environment is ready for the test suite
clean_test_environment() {
  # Use the BATS_TEST_NAME env var to categorise all logging messages relating
  # to the clean-up activities
  export BATS_TEST_NAME="clean_test_environment"

  _run base_cleanup "${CE_TEST_IMAGE}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  fi
}

# Runs once before the first test
setup_file() {
  _run test_suite_setup clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_setup clean_test_environment"
    return 1
  fi

  # Call without run as we might export environment variables
  extra_setup
  status="${?}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "extra_setup"
    return 1
  fi
}

# Runs after the final test
teardown_file() {
  _run test_suite_teardown clean_test_environment
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "test_suite_teardown clean_test_environment"
    return 1
  fi
}

# bats file_tags=soafee, openadkit
@test 'Check docker exist' {
  docker_binary=$(command -v docker 2>/dev/null || echo "")
  if [[ -z "${docker_binary}" ]]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test 'Check docker socket' {
  _run systemctl status -l docker.socket
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test "Check ${USER} user belong to docker group" {
  docker_group=$(id -nG | grep -ow docker || echo "")
  if [[ -z "${docker_group}" ]]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

# Pull the image used for the tests
@test "Pull Docker image" {
  _run retry 10 3 image_pull "${CE_TEST_IMAGE}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

# Run a container of the given image type pulled from an external image
# hub, to execute an example workload simply consisting of an interactive
# shell, then remove it afterwards.
@test "Run a container with a persistent workload" {
  _run image_run "-itd" "${CE_TEST_IMAGE}" "/bin/sh"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi

  # Save it here so the rest of tests can use the container id
  echo "${output}" > "$TEST_CONTAINER_ID"
}

@test "Check that the container is running" {
  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")
  _run check_container_is_running "${CONTAINER_ID}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test "Remove the running container" {
  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")
  _run container_remove "${CONTAINER_ID}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

@test "Check the container is not running" {
  CONTAINER_ID=$(cat "$TEST_CONTAINER_ID")
  _run check_container_is_not_running "${CONTAINER_ID}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}

# Run a non-detached container that executes a workload that requires
# network access. If the command does not return an error, then the
# container has network connectivity
@test 'Check container network connectivity' {
  engine_args="-it"
  workload="apk update"

  # Update apk package lists within container
  _run image_run "${engine_args}" "${CE_TEST_IMAGE}" "${workload}"
  if [ "${status}" -ne 0 ]; then
    log "FAIL" "${BATS_TEST_NAME}"
    return 1
  else
    log "PASS" "${BATS_TEST_NAME}"
  fi
}
