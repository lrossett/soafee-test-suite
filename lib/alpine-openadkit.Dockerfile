FROM arm64v8/alpine:3.17.2

RUN apk update && \
    apk add iproute2-minimal=6.0.0-r1 && \
    apk add iputils=20211215-r0 && \
    apk add stress-ng=0.14.00-r0
