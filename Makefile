PREFIX ?= /usr/local

LIB = lib
SOAFEE_DIR = share/soafee-test-suite

$(shell mkdir -p $(PREFIX)/$(SOAFEE_DIR)/$(LIB))

LIB_FILES += $(LIB)/container-engine-common-funcs.sh
LIB_FILES += $(LIB)/integration-tests-common-funcs.sh
LIB_FILES += $(LIB)/login-console-funcs.expect
LIB_FILES += $(LIB)/run-command.expect
LIB_FILES += $(LIB)/soafee-test-suite-lib.sh

PREFIX_LIB_FILES := $(addprefix $(PREFIX)/$(SOAFEE_DIR)/, $(LIB_FILES))

$(PREFIX_LIB_FILES): $(PREFIX)/$(SOAFEE_DIR)/% : %
	cp --no-preserve=ownership $< $@

.PHONY: install-lib
install-lib: $(PREFIX_LIB_FILES)

.PHONY: install
install: install-lib
	mkdir -p $(PREFIX)/$(SOAFEE_DIR)
	@# Don't keep ownership to avoid host contamination in Bitbake
	cp -R --no-preserve=ownership bin tests $(PREFIX)/$(SOAFEE_DIR)
	mkdir -p $(PREFIX)/bin
	@# Use relative symlink to avoid symlink-to-sysroot Bitbake warning
	cd $(PREFIX)/bin && \
	ln -s ../$(SOAFEE_DIR)/bin/soafee-test-suite soafee-test-suite && \
	ln -s ../$(SOAFEE_DIR)/bin/soafee-setup soafee-setup

.PHONY: uninstall
uninstall:
	rm -f $(PREFIX)/bin/soafee-test-suite
	rm -f $(PREFIX)/bin/soafee-setup
	rm -rf $(PREFIX)/$(SOAFEE_DIR)
